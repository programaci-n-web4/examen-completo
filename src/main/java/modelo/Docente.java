/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author emoru
 */
public class Docente {
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private double pagoBase;
    private int horasImpartidas;

    public Docente(int numDocente, String nombre, String domicilio, int nivel, double pagoBase, int horasImpartidas) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoBase = pagoBase;
        this.horasImpartidas = horasImpartidas;
    }

    public Docente() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    // Getters y Setters
    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public double getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(double pagoBase) {
        this.pagoBase = pagoBase;
    }

    public int getHorasImpartidas() {
        return horasImpartidas;
    }

    public void setHorasImpartidas(int horasImpartidas) {
        this.horasImpartidas = horasImpartidas;
    }

    // Métodos adicionales
    public double calcularPago() {
        double pago;

        switch (nivel) {
            case 1:
                pago = pagoBase * 1.3;
                break;
            case 2:
                pago = pagoBase * 1.5;
                break;
            case 3:
                pago = pagoBase * 2;
                break;
            default:
                pago = pagoBase;
        }

        return pago * horasImpartidas;
    }

    public double calcularImpuesto() {
        double pagoTotal = calcularPago();
        return pagoTotal * 0.16;
    }

    public double calcularBono(int cantidadHijos) {
        double bono;

        if (cantidadHijos <= 2) {
            bono = calcularPago() * 0.05;
        } else if (cantidadHijos >= 3 && cantidadHijos <= 5) {
            bono = calcularPago() * 0.1;
        } else {
            bono = calcularPago() * 0.2;
        }

        return bono;
    }
}

